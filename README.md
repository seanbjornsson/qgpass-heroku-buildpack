# QG Pass Heroku Buildpack

Based off of https://github.com/simon0191/custom-ssh-key-buildpack

Goal: install QGPass utility as part of the build process.
See https://devcenter.heroku.com/articles/quotaguardshield#https-proxy-with-qgpass
